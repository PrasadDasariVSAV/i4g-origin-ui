import {InjectionToken} from "@angular/core";
import {AppConfig} from "./app-config";

export const APP_CONFIG = new InjectionToken<AppConfig>('app-config');

export const ACCOUNTS_CONFIG: AppConfig = {
    apiEndpoint: 'http://34.241.175.223:8080/i4gorigin.accounts',
    httpAuthUser: 'raju',
    httpAuthPassword: 'raju',
    title: 'Dependency Injection'
};