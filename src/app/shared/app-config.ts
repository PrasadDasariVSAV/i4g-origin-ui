export interface AppConfig {
    apiEndpoint: string;
    httpAuthUser: string;
    httpAuthPassword: string;
    title: string;
}