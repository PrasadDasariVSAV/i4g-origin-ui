import {Component, OnInit, ViewChild} from '@angular/core';
import {
    CompanyInfo, CreateAccountWF, Country, CompanyCategory, Language, Currency, Response
} from "../account-data";
import {AccountService} from "../services/account.service";
import {NgModel, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn} from "@angular/forms";

@Component({
    selector: 'create-account',
    templateUrl: './create-account.html',
    styleUrls: ['./create-account.css'],
    providers: [ AccountService],
})

export class CreateAccountComponent implements OnInit {
    private _currentFlow : CreateAccountWF = CreateAccountWF.WHO_AM_I;
    private _company : CompanyInfo = new CompanyInfo();

    //look-up collections
    private _countries : Country[] = [];
    private _languages : Language[] = [];
    private _currencies : Currency[] = [];

    //form data
    private _currentCompanyCountry : Country;
    private _currentCompanyLanguage : Language;

    private _companyFormGroup : FormGroup;

    get name() { return this._companyFormGroup.get('name'); }
    get country() { return this._companyFormGroup.get('country'); }
    get email() {
        let em = this._companyFormGroup.get('email');
        return em;
    }
    get language() { return this._companyFormGroup.get('language');}
    get currency() { return this._companyFormGroup.get('currency');}
    get contactPersonName() { return this._companyFormGroup.get('contactPersonName');}
    get contactPersonSurname() { return this._companyFormGroup.get('contactPersonSurname');}
    get password() { return this._companyFormGroup.get('password');}
    get confirmPassword() { return this._companyFormGroup.get('confirmPassword');}
    get landLineCountryCode() { return this._companyFormGroup.get('landLineCountryCode');}
    get landLineAreaCode() { return this._companyFormGroup.get('landLineAreaCode');}
    get landLineNumber() { return this._companyFormGroup.get('landLineNumber');}
    get mobileCountryCode() { return this._companyFormGroup.get('mobileCountryCode');}
    get mobileAreaCode() { return this._companyFormGroup.get('mobileAreaCode');}
    get mobileNumber() { return this._companyFormGroup.get('mobileNumber');}
    get vMobileCountryCode() { return this._companyFormGroup.get('vMobileCountryCode');}
    get vMobileAreaCode() { return this._companyFormGroup.get('vMobileAreaCode');}
    get vMobileNumber() { return this._companyFormGroup.get('vMobileNumber');}
    get vEmail() { return this._companyFormGroup.get('vEmail');}
    get mobileOTP() { return this._companyFormGroup.get('mobileOTP');}
    get emailOTP() { return this._companyFormGroup.get('emailOTP');}

    constructor(private _service: AccountService) {
    }

    //on init, load lookups
    ngOnInit() {
        //load countries
        this._service.getCountries()
            .subscribe(countries => {
              this._countries = countries;
              this.populateCurrencies();
            });
        //load languages
        this._service.getLanguages()
            .subscribe(languages => this._languages = languages);

        //form validation
        this._companyFormGroup = new FormGroup({
            'name': new FormControl(this._company.name, Validators.required),
            'country': new FormControl(this._company.country, Validators.required),
            'email': new FormControl(this._company.email,
                [Validators.required,Validators.email],
                this.validateEmailNotRegistered.bind(this),
            ),
            'language': new FormControl(this._company.language, Validators.required),
            'currency': new FormControl(this._company.currency, Validators.required),
            'contactPersonName': new FormControl(this._company.contactPersonFirstName, Validators.required),
            'contactPersonSurname': new FormControl(this._company.contactPersonSurname, Validators.required),
            'password': new FormControl(this._company.password, Validators.required),
            'confirmPassword': new FormControl(
                this._company.confirmPassword,
                [Validators.required, validateConfirmedPassword(this._company)],
            ),
            'landLineCountryCode': new FormControl(this._company.landLineCountryCode, Validators.required),
            'landLineAreaCode': new FormControl(this._company.landLineAreaCode,
                [Validators.required,Validators.pattern('[0-9]{1,3}')]),
            'landLineNumber': new FormControl(this._company.landLineNumber, [
                Validators.required,
                Validators.pattern('[0-9]{5,10}')
            ]),
            'mobileCountryCode': new FormControl(this._company.mobileCountryCode, Validators.required),
            'mobileAreaCode': new FormControl(this._company.mobileAreaCode,
                [Validators.required, Validators.pattern('[0-9]{1,3}')]),
            'mobileNumber': new FormControl(this._company.mobileNumber,
                [Validators.required, Validators.pattern('[0-9]{5,10}')],
                this.validateMobileNotRegistered.bind(this)
            ),
            'vMobileCountryCode': new FormControl(this._company.mobileCountryCode, Validators.required),
            'vMobileAreaCode': new FormControl(this._company.mobileAreaCode, Validators.required),
            'vMobileNumber': new FormControl(this._company.mobileNumber,
                [Validators.required],
                this.validateMobileNotRegistered.bind(this)
            ),
            'vEmail': new FormControl(this._company.email,
                [Validators.required,Validators.email],
                this.validateEmailNotRegistered.bind(this)
            ),
            'mobileOTP': new FormControl(
                this._company.mobileOTP, [
                    Validators.required
                ]),
            'emailOTP': new FormControl(this._company.emailOTP, [
                Validators.required
            ])
        });
    }

    //populate currencies and filter duplicates
    populateCurrencies () {
        for (let country of this._countries) {
            this._currencies = this._currencies.concat(country.currencies);
        }
    }

    onCountryChange(value:string) {
        let cntry : Country = this.findItem(this._countries, 'name', value);
        if(cntry!=null) {
            this._company.countryObj = cntry;
            this._company.mobileCountryCode = cntry.callingCode;
            this._company.landLineCountryCode = cntry.callingCode;
        }
    }

    findItem (arr:any[], key:string, value:any) {
        let item : any = null;
        for (let i of arr) {
            if(i[key]==value){
                item = i;
                break;
            }
        }
        return item;
    }

    onLanguageChange(value:string) {
        let language : Language = this.findItem(this._languages, 'LanguageName', value);
        this._company.languageObj = language;
    }

    saveCompany() {
        this._service.saveCompanyInfo(this._company)
            .subscribe(resp => {
                let r : Response = resp;
                if (r.status==200) {
                    this._company.companyId = r.message;
                    this._service.sendVerifyEmailOTP(this._company.companyId);
                    this._service.sendVerifyMobileOTP(this._company.companyId);
                    this._currentFlow++;
                }
            });
    }

    updateCompany(controlGroup:string) {
        this._service.saveCompanyInfo(this._company)
            .subscribe(resp => {
                let r : Response = resp;
                if (r.status==200) {
                    if(controlGroup=='mobile') {
                        this.vMobileAreaCode.markAsPristine(true);
                        this.vMobileNumber.markAsPristine(true);
                        this._service.sendVerifyMobileOTP(this._company.companyId);
                    }
                    if(controlGroup=='email') {
                        this.vEmail.markAsPristine(true);
                        this._service.sendVerifyEmailOTP(this._company.companyId);
                    }
                }
            });
    }

    sendVerifyEmailOTP() {
        this._service.sendVerifyEmailOTP(this._company.companyId);
    }

    sendVerifyMobileOTP() {
        this._service.sendVerifyMobileOTP(this._company.companyId);
    }

    validateEmailVerificationCode() {
        this._service.validateEmailVerificationCode(this._company.emailOTP, this._company.companyId)
            .subscribe(resp => this._company.response = resp);
    }

    validateMobileVerificationCode () {
        this._service.validateMobileVerificationCode(this._company.mobileOTP, this._company.companyId)
            .subscribe(resp => {
                this._company.response = resp;
            });
    }

    selectWhoAmI(isLocalCompany:boolean) {
        this._company.localCompany = isLocalCompany;
        if(isLocalCompany){
            this._company.category = CompanyCategory.SELLER;
        } else {
            this._company.category = CompanyCategory.BUYER;
        }
    }


    // Navigation functions
    selectNextFlow() {
        if(this._currentFlow==CreateAccountWF.COMPANY_INFO) {
            this.saveCompany();
            return;
        }
        this._currentFlow++;
    }

    selectPreviousFlow() {
        this._currentFlow--;
    }

    //Validators
    validateEmailNotRegistered(control: AbstractControl) {
        return this._service.isEmailRegistered(control.value).map(
            res => {
                let emailTaken = res.message!='false';
                return emailTaken? {emailTaken: true}: null;
            });
    }

    validateMobileNotRegistered(control: AbstractControl) {
        let mobileNm = this._company.mobileAreaCodeAndNumber();
        return this._service.isMobileRegistered(this._company.mobileCountryCode, mobileNm).map(
            res => {
                let mobileTaken = res.message!='false';
                return mobileTaken? {mobileTaken: true}: null;
            });
    }

    validateConfirmedPassword(control: AbstractControl) {
        let confirmed : boolean = this._company.password==control.value;
        if(confirmed) {
            return null;
        }else {
            return {notConfirmed: true};
        }
    }

    isCompanyProfileFormValid() {
        return this.name.valid &&
                this.country.valid &&
                this.email.valid &&
                this.language.valid &&
                this.currency.valid &&
                this.contactPersonName.valid &&
                this.contactPersonSurname.valid &&
                this.password.valid &&
                this.confirmPassword.valid &&
                this.landLineCountryCode.valid &&
                this.landLineAreaCode.valid &&
                this.landLineNumber.valid &&
                this.mobileCountryCode.valid &&
                this.mobileAreaCode.valid &&
                this.mobileNumber.valid;
    }
}


/** custome validation */
function validateConfirmedPassword(company: CompanyInfo): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const confirmed = company.password==control.value;
        return confirmed ? null :  {'notConfirmed': true};
    };
}
