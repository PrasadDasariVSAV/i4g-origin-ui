import {error} from "util";
export class CompanyInfo {
    companyId : string;
    localCompany : Boolean  = true;
    name : string;
    category : CompanyCategory = CompanyCategory.BUYER;
    email : string;
    password : string;
    confirmPassword : string;
    landLineCountryCode : string;
    landLineAreaCode : string;
    landLineNumber : string;
    country : string;
    countryObj : Country;
    mobileCountryCode : string;
    mobileAreaCode : string;
    mobileNumber : string;
    mobileVerified : boolean;
    emailVerified : boolean;
    emailOTP : string;
    mobileOTP : string;
    contactPersonFirstName : string;
    contactPersonSurname : string;
    companyCode : string;
    canReceiveSMS : boolean = true;
    canReceiveEmail : boolean = true;
    agreedToTermsAndConditions : boolean = false;
    language : string;
    languageObj : Language;
    response : Response;
    currency : string;

    mobileAreaCodeAndNumber () {
        return this.mobileAreaCode +''+ this.mobileNumber;
    }

    landlineAreaCodeAndNumber () {
        return this.landLineAreaCode + this.landLineNumber;
    }

    getPayload() : CompanyProfile {
        let profile: CompanyProfile = new CompanyProfile();
        profile.AgreeTermsAndConditions = this.agreedToTermsAndConditions? 'Y':'N';
        profile.AgreeToRecieveEmail = this.canReceiveEmail? 'Y':'N';
        profile.AgreeToRecieveSMS = this.canReceiveSMS? 'Y':'N';
        profile.BusinessLocation = this.countryObj.name;
        profile.Code = this.companyCode;
        profile.CompanyBusinessLandlineCode = this.landLineCountryCode;
        profile.CompanyBusinessLandlineNumber = this.landLineAreaCode + this.landLineNumber;
        profile.CompanyBusinessMobileCode = this.mobileCountryCode;
        profile.CompanyBusinessMobileNumber = this.mobileAreaCode + this.mobileNumber;
        profile.CompanyCode = this.companyCode;
        profile.CompanyContactPersonFirstName = this.contactPersonFirstName;
        profile.CompanyContactPersonSurname = this.contactPersonSurname;
        profile.CompanyCurrency = this.currency;
        profile.CompanyDefaultLanguage = this.languageObj.LanguageName;
        profile.CompanyEmail = this.email;
        profile.CompanyName = this.name;
        profile.CompanyType = this.category==CompanyCategory.BUYER? '1':'2';
        profile.Password = this.password;
        profile.RegisteredBy = 'Buyer';
        profile.CompanyID = this.companyId;
        return profile;
    }
}

export class CompanyProfile {
    AgreeTermsAndConditions: string;
    AgreeToRecieveEmail: string;
    AgreeToRecieveSMS: string;
    BusinessLocation: string;
    Code: string;
    CompanyBusinessLandlineCode: string;
    CompanyBusinessLandlineNumber: string;
    CompanyBusinessMobileCode: string;
    CompanyBusinessMobileNumber: string;
    CompanyCode: string;
    CompanyContactPersonFirstName: string;
    CompanyContactPersonSurname: string;
    CompanyCurrency: string;
    CompanyDefaultLanguage: string;
    CompanyEmail: string;
    CompanyID: string;
    CompanyName: string;
    CompanyType: string;
    Password: string;
    RegisteredBy: string;
}

export class Country {
    name: string;
    alpha2Code: string;
    alpha3Code: string;
    callingCode: string;
    currencies: Currency[];
}

export class Language {
    LanguageID : string;
    LanguageName : string;
}

export class Currency {
    code: string;
    name: string;
}

export enum CreateAccountWF {
    WHO_AM_I,
    COMPANY_INFO,
    VERIFICATION
}

export enum CompanyCategory {
    SELLER, BUYER
}

export class Response {
    error : string;
    exception: string;
    message : string;
    path : string;
    status : number;
    timestamp : number;
}