import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {CompanyInfo, CompanyProfile, CompanyCategory} from "../account-data";

@Injectable()
export class AccountService {
    private _endpointUrl : string;
    private _headers : Headers;

    constructor(private http: Http) {
        this._endpointUrl = 'http://34.241.175.223:8080/i4gorigin.accounts';//config.apiEndpoint;
        this._headers = new Headers();
        this._headers.append('Authorization', 'Basic  '+ btoa('raju:raju'));
    }

    saveCompanyInfo(company:CompanyInfo) {
        if(company.category == CompanyCategory.BUYER) {
          console.log(" Saving Buyer Profile  ======> " , company.category);
            return this.http.post(this._endpointUrl+'/saveBuyerProfile', {Company:company.getPayload()}, new RequestOptions({ headers: this._headers }))
            .map(res => res.json());
        } else {
          console.log(" Saving Seller Profile  ======> " , company.category);
             this.http.post(this._endpointUrl+'/saveSellerProfile', {Company:company.getPayload()}, new RequestOptions({ headers: this._headers }))
                .map(res => res.json());
        }

    }
  /*
   IsEmailRegistered
   */
  isEmailRegistered(email:string) {
   console.log("Prasad : Rest service call : isEmailRegistered ===>  ", this._endpointUrl+'/isEmailRegistered', "has got company Email Address : ", email);
   return this.http.post(this._endpointUrl+'/isEmailRegistered', {email:email}, new RequestOptions({ headers: this._headers }))
     .map(res => res.json());

   }

 /*
  IsMobile Registered
  */
   isMobileRegistered(code:string, mobileNumber:string) {
   console.log("Prasad : Rest service call :  isMobileRegistered  ===>  ", this._endpointUrl+'/isMobileRegistered', "has got code : ", code, " & mobile number :", mobileNumber);
   return this.http.post(this._endpointUrl+'/isMobileRegistered', {code:code,number:mobileNumber}, new RequestOptions({ headers: this._headers }))
     .map(res => res.json());
   }

  /*
   sendVerifyEmailOTP
   */
   sendVerifyEmailOTP(companyId:string) {
     console.log("Prasad : Rest service call for  sendVerifyEmail ===>  ", this._endpointUrl+'/sendVerifyEmail', "has got company Id : ", companyId);
     return this.http.post(this._endpointUrl+'/sendVerifyEmail', {Company:{CompanyID:companyId}}, new RequestOptions({ headers: this._headers }))
       .map(res => res.json());
  }

  /*
  sendVerifyMobileOTP
   */
  sendVerifyMobileOTP(companyId:string) {
    console.log(" Prasad : Rest service call for sendVerifyMobile ===>  ", this._endpointUrl+'/sendVerifyMobile', "has got company Id : ", companyId);
    return this.http.post(this._endpointUrl+'/sendVerifyMobile', {Company:{CompanyID:companyId}}, new RequestOptions({ headers: this._headers }))
      .map(res => res.json());
  }

  /*
   validateEmailVerificationCode()
   */

  validateEmailVerificationCode(code:string,companyId:string) {
    console.log("Prasad : Rest service call for  validateEmailVerificationCode() ===>  ", this._endpointUrl+'/validateEmailVerificationCode', "has got code & company Id as : ", +code + "&" +companyId);
    return this.http.post(this._endpointUrl+'/validateEmailVerificationCode', {Company:{Code:code,CompanyID:companyId}}, new RequestOptions({ headers: this._headers }))
      .map(res => res.json());

  }

  /*
   validateEmailVerificationCode()
   */

  validateMobileVerificationCode(code:string,companyId:string) {
    console.log("Prasad : Rest service call for  validateMobileVerificationCode() ===>  ", this._endpointUrl+'/validateMobileVerificationCode', "has got code & company Id as : ", +code + "&" +companyId);
    return this.http.post(this._endpointUrl+'/validateMobileVerificationCode', {Company:{Code:code,CompanyID:companyId}}, new RequestOptions({ headers: this._headers }))
      .map(res => res.json());

  }
    /*
    lookups
     */
    getCountries(): Observable<any> {
        return this.http.get(this._endpointUrl+'/getCountriesList',new RequestOptions({ headers: this._headers }))
            .map(res => res.json().Countries.Country)
    }

    getLanguages(): Observable<any> {
        return this.http.get(this._endpointUrl+'/getLanguagesList',new RequestOptions({ headers: this._headers }))
            .map(res => res.json().Languages.Language)
    }
}
