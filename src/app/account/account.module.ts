import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SubHeaderComponent } from "../common/sub-header/sub-header";
import { CreateAccountComponent } from "./create-account/create-account";
import {AccountComponent} from "./account";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

@NgModule({
    declarations: [
        CreateAccountComponent,
        SubHeaderComponent, AccountComponent
    ],
    imports: [
        BrowserModule, FormsModule, HttpModule,
        ReactiveFormsModule
    ],
    exports: [
        AccountComponent, SubHeaderComponent, CreateAccountComponent
    ]
})
export class AccountModule {

}
